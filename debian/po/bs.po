# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_bs.po to Bosnian
# Bosnian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Safir Secerovic <sapphire@linux.org.ba>, 2006.
# Armin Besirovic <armin@linux.org.ba>, 2008.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Free Software Foundation, Inc., 2001,2002,2003,2004
#   Safir Šećerović <sapphire@linux.org.ba>, 2004,2006.
#   Vedran Ljubovic <vljubovic@smartnet.ba>, 2001
#   (translations from drakfw).
#   Translations from KDE:
#   Nesiren Armin <bianchi@lugbih.org>, 2002
#   Vedran Ljubovic <vljubovic@smartnet.ba>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_bs\n"
"Report-Msgid-Bugs-To: partman-nbd@packages.debian.org\n"
"POT-Creation-Date: 2015-10-28 22:02+0000\n"
"PO-Revision-Date: 2014-05-21 11:20+0100\n"
"Last-Translator: Amila Valjevčić <valjevcic.amila@gmail.com>\n"
"Language-Team: Bosnian <lokal@linux.org.ba>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: 3;\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-nbd.templates:1001
msgid "Configure the Network Block Device"
msgstr "Podesi Network Block uređaj"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "NBD configuration action:"
msgstr "NBD konfiguracijska akcija:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "There are currently ${NUMBER} devices connected."
msgstr "Trenutno ima ${NUMBER} prikopčanih uređaja."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid "Network Block Device server:"
msgstr "Server Network Block uređaja:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid ""
"Please enter the host name or the IP address of the system running nbd-"
"server."
msgstr ""
"Molim unesite hostname ili IP adresu sistema na kom je pokrenut nbd-server."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid "Name for NBD export"
msgstr ""

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid ""
"Please enter the NBD export name needed to access nbd-server. The name "
"entered here should match an existing export on the server."
msgstr ""

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid "Network Block Device device node:"
msgstr "Čvor Network Block uređaja:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid ""
"Please select the NBD device node that you wish to connect or disconnect."
msgstr ""
"Molim odaberite čvor NBD uređaja koji želite priključiti ili iskopčati."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
msgid "Failed to connect to the NBD server"
msgstr "Ne mogu se konektovati na NBD server"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
#, fuzzy
msgid ""
"Connecting to the nbd-server failed. Please ensure that the host and the "
"export name which you entered are correct, that the nbd-server process is "
"running on that host, that the network is configured correctly, and retry."
msgstr ""
"Konektovanje na nbd-server neuspjelo. Molimo provjerite da je host i port "
"ili naziv koji ste unijeli tačan, da je nbd-server proces pokrenut na tom "
"hostu i portu (ili koristi taj naziv), da je mreža pravilno podešena i "
"pokušajte opet."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid "No more Network Block Device nodes left"
msgstr "Nema preostalih Network Block uređaja."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"Either all available NBD device nodes are in use or something went wrong "
"with the detection of the device nodes."
msgstr ""
"Ili su svi dostupni čvorovi NBD uređaja iskorišteni ili je došlo do greške "
"pri otkrivanju čvorova uređaja."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"No more NBD device nodes can be configured until a configured one is "
"disconnected."
msgstr ""
"Nije moguće podesiti više čvorova NBD uređaja dok se ne iskopča neki od "
"podešenih."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid "No connected Network Block Device nodes were found"
msgstr "Nije pronađen nijedan prikopčan Network Block uređaj."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid ""
"There are currently no Network Block Device nodes connected to any server. "
"As such, you can't disconnect any of them."
msgstr ""
"Trenutno nema nijedan Network Block uređaj prikopčan na bilo koji server. "
"Kao posljedica, ne možete isključiti nijedan uređaj."

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:9001
msgid "Connect a Network Block Device"
msgstr "Priključi Network Block uređaj"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:10001
msgid "Disconnect a Network Block Device"
msgstr "Iskopčaj Network Block uređaj"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:11001
msgid "Finish and return to the partitioner"
msgstr "Završi i vrati se na particionisanje"
